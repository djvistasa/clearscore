import styled from 'styled-components';
import Dialog from '@material-ui/core/Dialog';
import { calculateRem } from '../../../utils/helpers';
import breakpoint from 'styled-components-breakpoint';

// NOTE: change yourElementType to your preferred type: e.g button
const StyledModal = styled(Dialog)`
  .MuiDialog-paper {
    min-width: ${calculateRem(300)};
    padding: ${calculateRem(20)};
    box-sizing: border-box;

    h1 {
      text-align: center;
      margin-bottom: ${calculateRem(20)};
    }

    .MuiTextField-root {
      margin-bottom: ${calculateRem(20)};
    }

    ${breakpoint('tablet')`
      min-width: ${calculateRem(500)};
    `}
    ${breakpoint('tabletPlus')`
      min-width: ${calculateRem(600)};
    `}
  }
`;

const StyledActionsWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;

  /* margin-top: ${calculateRem(20)}; */
`;

export { StyledModal, StyledActionsWrapper };
