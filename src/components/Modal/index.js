/**
 *
 * Modal
 *
 */

import React, { memo, useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
// import PropTypes from 'prop-types';
import {
  StyledModal,
  StyledActionsWrapper,
} from './styledComponents/styledModal';

const Modal = props => {
  const { primaryAction, secondaryAction } = props;

  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  const [isTitleDirty, setIsTitleDirty] = useState(false);
  const [isDescriptionDirty, setIsDescriptionDirty] = useState(false);

  const handleSecondaryAction = () => {
    if (secondaryAction && typeof secondaryAction === 'function') {
      secondaryAction();
    }
  };

  const handlePrimaryAction = () => {
    if (description.length < 1 || title.length < 1) {
      setIsTitleDirty(true);
      setIsDescriptionDirty(true);
      return;
    }
    if (primaryAction && typeof primaryAction === 'function') {
      primaryAction({
        title,
        createdDate: Date.now(),
        updatedDate: Date.now(),
        description,
      });
    }
  };

  const handleTitleChange = element => {
    setIsTitleDirty(true);
    setTitle(element.target.value);
  };

  const handleDescriptionChange = element => {
    const value = element.target.value;
    setIsDescriptionDirty(true);

    if (value.length <= 140) {
      setDescription(element.target.value);
    }
  };
  return (
    <StyledModal open>
      <h1>Create new Idea</h1>
      <TextField
        name="title"
        onChange={handleTitleChange}
        type="text"
        autoFocus
        error={isTitleDirty && title.length < 1}
        value={title}
        variant="outlined"
        placeholder="Enter title"
        label={isTitleDirty && title.length < 1 ? 'Required' : 'Enter title'}
        required
      />
      <TextField
        title="description"
        onChange={handleDescriptionChange}
        type="text"
        error={isDescriptionDirty && description.length < 1}
        value={description}
        rows="4"
        rowsMax="4"
        variant="outlined"
        placeholder="Enter description"
        label={
          isDescriptionDirty && description.length < 1
            ? 'Required'
            : 'Enter description'
        }
        multiline
        required
        helperText={`${description.length}/140`}
      />
      <StyledActionsWrapper>
        <Button
          variant="outlined"
          color="secondary"
          onClick={handleSecondaryAction}
        >
          Cancel
        </Button>
        <Button
          variant="outlined"
          color="primary"
          onClick={handlePrimaryAction}
        >
          Create
        </Button>
      </StyledActionsWrapper>
    </StyledModal>
  );
};

Modal.propTypes = {};

export default memo(Modal);
