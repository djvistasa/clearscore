import styled from 'styled-components';
import { calculateRem } from '../../../utils/helpers';
import { responsiveTheme } from '../../../utils/responsiveTheme';
import Card from '@material-ui/core/Card';
// import breakpoint from 'styled-components-breakpoint';

// NOTE: change yourElementType to your preferred type: e.g button
const StyledIdea = styled(Card)`
  padding: ${calculateRem(20)};
  box-sizing: border-box;

  .MuiTextField-root {
    width: 100%;
    margin-bottom: ${calculateRem(20)};
  }
`;

const StyledActionsWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const StyledDateWrapper = styled.div`
  h1 {
    font-size: ${calculateRem(12)};
    color: ${responsiveTheme.colors.grey};
    &:first-child {
      margin-bottom: ${calculateRem(5)};
    }
  }
  margin-bottom: ${calculateRem(20)};
`;

export { StyledIdea, StyledActionsWrapper, StyledDateWrapper };
