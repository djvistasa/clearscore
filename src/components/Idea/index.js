/**
 *
 * Idea
 *
 */

import React, { memo, useState } from 'react';
import Button from '@material-ui/core/Button';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import Moment from 'react-moment';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';
import {
  StyledIdea,
  StyledActionsWrapper,
  StyledDateWrapper,
} from './styledComponents/styledIdea';

const Idea = props => {
  const { idea, handleEdit, handleDelete } = props;
  const [title, setTitle] = useState(idea.title);
  const [description, setDescription] = useState(idea.description);
  const [isTitleDirty, setIsTitleDirty] = useState(false);
  const [isDescriptionDirty, setIsDescriptionDirty] = useState(false);

  const handleDeleteIdea = () => {
    const { id } = idea;

    handleDelete(id);
  };

  const handleEditIdea = () => {
    const { id } = idea;
    if (description.length < 1 || title.length < 1) {
      setIsTitleDirty(true);
      setIsDescriptionDirty(true);
      return;
    }
    handleEdit({
      id,
      title,
      description,
    });
  };

  const handleTitleChange = element => {
    setIsTitleDirty(true);
    setTitle(element.target.value);
  };

  const handleDescriptionChange = element => {
    const value = element.target.value;
    setIsDescriptionDirty(true);

    if (value.length <= 140) {
      setDescription(element.target.value);
    }
  };
  return (
    <StyledIdea>
      <StyledDateWrapper>
        <h1>
          Created: <Moment fromNow>{idea.createdDate}</Moment>
        </h1>
        <h1>
          Last Modified: <Moment fromNow>{idea.updatedDate}</Moment>
        </h1>
      </StyledDateWrapper>
      <TextField
        name="title"
        variant="outlined"
        value={title}
        error={isTitleDirty && title.length < 1}
        label={isTitleDirty && title.length < 1 ? 'Required' : 'Edit title'}
        onChange={handleTitleChange}
      />
      <TextField
        name="description"
        value={description}
        variant="outlined"
        multiline
        rows="4"
        rowsMax="4"
        error={isDescriptionDirty && description.length < 1}
        label={
          isDescriptionDirty && description.length < 1
            ? 'Required'
            : 'Edit description'
        }
        onChange={handleDescriptionChange}
        helperText={`${description.length}/140`}
      />
      <StyledActionsWrapper>
        <Button
          variant="outlined"
          color="primary"
          startIcon={<EditIcon />}
          onClick={handleEditIdea}
        >
          Update
        </Button>
        <Button
          variant="outlined"
          color="primary"
          startIcon={<DeleteIcon />}
          onClick={handleDeleteIdea}
        >
          Delete
        </Button>
      </StyledActionsWrapper>
    </StyledIdea>
  );
};

Idea.propTypes = {
  idea: PropTypes.object,
  handleEdit: PropTypes.func,
  handleDelete: PropTypes.func,
};

export default memo(Idea);
