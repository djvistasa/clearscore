/**
 *
 * NavBar
 *
 */

import React from 'react';
import {
  StyledNavBar,
  StyledNavBarInner,
} from './styledComponents/styledNavBar';

const NavBar = () => (
  <StyledNavBar>
    <StyledNavBarInner>
      <h1>Idea Board</h1>
    </StyledNavBarInner>
  </StyledNavBar>
);

export default NavBar;
