import styled from 'styled-components';
import AppBar from '@material-ui/core/AppBar';
import { calculateRem } from '../../../utils/helpers';
// import breakpoint from 'styled-components-breakpoint';

// NOTE: change yourElementType to your preferred type: e.g button
const StyledNavBar = styled(AppBar)`
  padding: ${calculateRem(20)};
  box-sizing: border-box;
`;

const StyledNavBarInner = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  max-width: ${calculateRem(1200)};
  width: 100%;
  margin: auto;
`;

export { StyledNavBar, StyledNavBarInner };
