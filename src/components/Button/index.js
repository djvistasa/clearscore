/**
 *
 * Button
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import StyledButton from './styledComponents/styledButton';

const Button = props => {
  const { title, disabled, onClick, type, startIcon } = props;

  const handleClick = () => {
    if (typeof onClick === 'function' && onClick) {
      onClick();
    }
  };
  return (
    <StyledButton
      type={type}
      disabled={disabled}
      onClick={handleClick}
      startIcon={startIcon}
    >
      {title}
    </StyledButton>
  );
};

Button.propTypes = {
  onClick: PropTypes.func,
  disabled: PropTypes.bool,
  secondaryButton: PropTypes.bool,
  title: PropTypes.string.isRequired,
  type: PropTypes.string,
};

export default Button;
