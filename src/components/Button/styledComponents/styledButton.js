import styled from 'styled-components';
import Button from '@material-ui/core/Button';
import { responsiveTheme } from '../../../utils/responsiveTheme';
// import { calculateRem } from '../../../utils/helpers';

// NOTE: change yourElementType to your preferred type: e.g button
const StyledButton = styled(Button)`
  &:hover {
    background-color: ${responsiveTheme.colors.mustard};
  }
  &:disabled {
    opacity: 0.4;
  }
`;

/** @component */
export default StyledButton;
