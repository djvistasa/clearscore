import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { useStoreState } from 'easy-peasy';
import NavBar from './components/NavBar';
import Modal from './components/Modal';
import IdeaBoard from './containers/IdeaBoard';

function App() {
  const modalConfig = useStoreState(state => state.modal.config);

  const { isModalOpen, primaryAction, secondaryAction } = modalConfig;
  return (
    <>
      <NavBar />
      {isModalOpen && (
        <Modal
          primaryAction={primaryAction}
          secondaryAction={secondaryAction}
        />
      )}

      <Switch>
        <Route path="/" component={IdeaBoard} />
      </Switch>
    </>
  );
}

export default App;
