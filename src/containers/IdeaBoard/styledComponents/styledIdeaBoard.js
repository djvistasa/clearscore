import styled from 'styled-components';
import breakpoint from 'styled-components-breakpoint';
import { calculateRem } from '../../../utils/helpers';

const StyledIdeaBoard = styled.div`
  max-width: ${calculateRem(1200)};
  margin: ${calculateRem(57)} auto 0;
  display: ${props => (props.hasNoIdeas ? 'block' : 'grid')};
  grid-gap: ${calculateRem(20)};
  padding: ${calculateRem(20)};

  .create-idea,
  .sort-alphabetically,
  .sort-created {
    position: fixed;
    bottom: ${calculateRem(20)};
    right: ${calculateRem(20)};
    cursor: pointer;
  }

  .sort-alphabetically {
    right: ${calculateRem(96)};
  }

  ${breakpoint('maxWidth')`
        grid-template-columns: repeat(2, 1fr);
    `};
  ${breakpoint('tabletPlus')`
        grid-template-columns: repeat(3, 1fr);
    `};
`;

const StyledNoIdeasWrapper = styled.div`
  margin-top: 100px;
  text-align: center;
`;

export { StyledIdeaBoard, StyledNoIdeasWrapper };
