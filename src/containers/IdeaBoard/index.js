/**
 *
 * IdeaBoard
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
import { useStoreState, useStoreActions } from 'easy-peasy';
import AddIcon from '@material-ui/icons/Add';
import SortByAlphaIcon from '@material-ui/icons/SortByAlpha';
import Fab from '@material-ui/core/Fab';
import { useSnackbar } from 'notistack';
import { handleModal } from '../../utils/helpers';
import {
  StyledIdeaBoard,
  StyledNoIdeasWrapper,
} from './styledComponents/styledIdeaBoard';
import Idea from '../../components/Idea';

function IdeaBoard() {
  const ideas = useStoreState(state => state.ideaBoard.ideas);
  const addIdea = useStoreActions(actions => actions.ideaBoard.add);
  const deleteIdea = useStoreActions(actions => actions.ideaBoard.delete);
  const editIdea = useStoreActions(actions => actions.ideaBoard.edit);
  const sortAlphabetically = useStoreActions(
    actions => actions.ideaBoard.sortAlphabetically,
  );
  const { enqueueSnackbar } = useSnackbar();

  const handleAddIdea = idea => {
    addIdea({
      id: Date.now(),
      ...idea,
    });
    enqueueSnackbar('New Idea has been created', { variant: 'success' });
  };

  const handleEditIdea = idea => {
    editIdea(idea);
    enqueueSnackbar('Idea has been updated', { variant: 'info' });
  };

  const handleDeleteIdea = ideaId => {
    deleteIdea(ideaId);
    enqueueSnackbar('Idea has been deleted', { variant: 'info' });
  };

  return (
    <StyledIdeaBoard hasNoIdeas={ideas.length === 0}>
      {ideas && ideas.length > 0 ? (
        ideas.map(idea => (
          <Idea
            handleEdit={handleEditIdea}
            handleDelete={handleDeleteIdea}
            key={idea.id}
            idea={idea}
          />
        ))
      ) : (
        <StyledNoIdeasWrapper>
          You don't have any ideas yet, press the + button on the bottom right
          to begin
        </StyledNoIdeasWrapper>
      )}

      <Fab
        color="primary"
        className="create-idea"
        aria-label="add"
        onClick={() => handleModal(handleAddIdea)}
      >
        <AddIcon />
      </Fab>
      <Fab
        color="secondary"
        className="sort-alphabetically"
        aria-label="sort-alpha"
        onClick={sortAlphabetically}
      >
        <SortByAlphaIcon />
      </Fab>
    </StyledIdeaBoard>
  );
}

IdeaBoard.propTypes = {};

export default IdeaBoard;
