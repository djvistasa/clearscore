import { createStore, action, persist } from 'easy-peasy';

export const store = createStore({
  ideaBoard: persist({
    ideas: [
      {
        id: 0,
        title: 'test',
        createdDate: Date.now(),
        updatedDate: Date.now(),
        description: 'test',
      },
    ],
    add: action((state, payload) => {
      state.ideas.push(payload); // eslint-disable-line
    }),
    delete: action((state, payload) => {
      const itemsWithoutItemToBeRemoved = state.ideas.filter(
        idea => idea.id !== payload,
      );
      state.ideas = itemsWithoutItemToBeRemoved; // eslint-disable-line
    }),
    edit: action((state, payload) => {
      const itemToEdit = state.ideas.filter(idea => idea.id === payload.id);

      if (itemToEdit.length > 0) {
        itemToEdit[0].title = payload.title;
        itemToEdit[0].description = payload.description;
        itemToEdit[0].updatedDate = Date.now();
      }

      // state.ideas = itemsWithoutItemToBeRemoved; // eslint-disable-line
    }),
    sortAlphabetically: action((state, payload) => {
      const sortedIdeas = state.ideas.sort((a, b) =>
        a.title.localeCompare(b.title),
      );
      state.ideas = sortedIdeas; // eslint-disable-line
    }),
  }),
  modal: {
    config: {
      isModalOpen: false,
      primaryAction: null,
      secondaryAction: null,
    },
    toggleModal: action((state, payload) => {
      state.config.isModalOpen = payload; // eslint-disable-line
    }),
    setPrimaryAction: action((state, payload) => {
      state.config.primaryAction = payload; // eslint-disable-line
    }),
    setSecondaryAction: action((state, payload) => {
      state.config.secondaryAction = payload; // eslint-disable-line
    }),
  },
});
