import { store } from './store';

export const composeValidators = (...validators) => value =>
  validators.reduce((error, validator) => error || validator(value), undefined);

export const calculateRem = size => `${(size / 16) * 1}rem`;

const storeActions = store.getActions();

export const handleModal = (action = () => {}) => {
  storeActions.modal.toggleModal(true);
  storeActions.modal.setPrimaryAction(val => {
    action(val);
    storeActions.modal.toggleModal(false);
  });
  storeActions.modal.setSecondaryAction(() => {
    storeActions.modal.toggleModal(false);
  });
};

export const handleApiError = error => {
  const err =
    error && error.response && error.response.data
      ? error.response.data
      : error;

  return {
    ok: false,
    result: null,
    err,
  };
};

export const handleApiSuccess = result => ({
  ok: true,
  result,
  error: null,
});
