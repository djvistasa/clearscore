# Developer Comments

There is some code repetition in the components Modal and Idea. Need to move that into a potential "IdeaForm" component

I used easy-peazy for state management because its very easy to plug in and is very powerful out the box

I also used global state to drive parts of the UI, like the modal for instance. I feel this is a clean, powerful approach that reduces html duplication

I used plop for scaffolding, I found that it saves me quite a bit of time and ensures that all my components follow the same structure

I used styled-components for css and rewired the app

I used material ui since I didn't have any design direction

I don't have any production experience writing unit tests so I chose not to attempt it and focus on the things that I knew I could do. I am still undecided about what it is one tests for on the UI and have read a few articles about react testing library. Maybe we could discuss this?

Thank you for this opportunity, I look forward to hearing from you.
